jQuery(document).ready(function () {
	$('#addEpic').modal({
		show: false
	});
	$('[id^="draggable_"]').draggable({ 
  		revert: "invalid"
  	});
	$('[id^="droppable_"]').droppable({
		hoverClass: "ui-state-active",
		drop: function( event, ui ) {
	        var $draggable = $(ui.draggable).addClass( "wasDragged" );
			$(".alert-success")
				.addClass( "ui-state-highlight" )
				.find( "p" )
					.html( "Dropped!" );
			$( this )
				.addClass( "submit_form" );
			$(".submit_form")
				.find( "form" )
					.append( '<input type="hidden" name="story_key" value="' + $('td.wasDragged .story_key').text() + '">' )
					.append( '<input type="hidden" name="link_id" value="' + $('td.wasDragged .link_id').text() + '">' )
				.submit()
			}
	});
});
