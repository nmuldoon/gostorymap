require 'sinatra'
require 'sinatra/content_for'

class Gostorymap < Sinatra::Base
  enable :sessions

  # Allows includes to be specified in erb's and placed in the layout.erb
  helpers Sinatra::ContentFor

  # This section gets called before every request. Here, we set up the
  # OAuth consumer details including the consumer key, private key,
  # site uri, and the request token, access token, and authorize paths
  before do
    session[:oauth] ||= {}

    # Set up the consumer
    @consumer ||= OAuth::Consumer.new(
      'gostorymap', # CONSUMER KEY 
      OpenSSL::PKey::RSA.new(IO.read(File.dirname(__FILE__) + "/keys/rsa.pem")),
      {
      :site => 'http://localhost:8080', # Set to localhost for development
      :signature_method => 'RSA-SHA1',
      :scheme => :header,
      :http_method => :post,
      :request_token_path=> '/plugins/servlet/oauth/request-token', 
      :access_token_path => '/plugins/servlet/oauth/access-token',
      :authorize_path => '/plugins/servlet/oauth/authorize'
    })

    # This logs the HTTP req/resp to the application's log. Useful for debugging the requests
    @consumer.http.set_debug_output($stderr)

    # Here we store the request token/secret in the session cookie
    if !session[:oauth][:request_token].nil? && !session[:oauth][:request_token_secret].nil?
      @request_token = OAuth::RequestToken.new(@consumer, session[:oauth][:request_token], session[:oauth][:request_token_secret])
    end

    # In the real world, you'll want to store the access token inside a datastore so as
    # to prevent your app from continually asking the user to approve/deny the authorization.
    if !session[:oauth][:access_token].nil? && !session[:oauth][:access_token_secret].nil?
      @access_token = OAuth::AccessToken.new(@consumer, session[:oauth][:access_token], session[:oauth][:access_token_secret])
    end
  end

  # Starting point: http://<yourserver>/
  # This will serve up a login link if you're not logged in. If you are, it'll show some user info and a
  # signout link
  get '/' do
    if !session[:oauth][:access_token]
      # not logged in
      erb :index
    else
      #logged in
      
      # You'll need to sign each request to the API by using @access_token.<method>.
      # Once we have the response, we grab the body then run it through a JSON decoder
      # user = MultiJson.decode(@access_token.get('/rest/auth/latest/session',
      #                                           {'Accept' => 'application/json'}).body)
      # server = MultiJson.decode(@access_token.get('/rest/api/latest/serverInfo',
      #                                             {'Accept' => 'application/json'}).body)

      @issues = MultiJson.decode(@access_token.get('/rest/api/2/search',
                                                {'Accept' => 'application/json'}).body)
      @projects = MultiJson.decode(@access_token.get('/rest/api/2/project',
                                                {'Accept' => 'application/json'}).body)
      erb :home
      
    end
  end

  # http://<yourserver>/signin
  # Initiates the OAuth dance by first requesting a token then redirecting to 
  # http://<yourserver>/auth to get the @access_token
  get '/signin' do
    @request_token = @consumer.get_request_token(:oauth_callback => "http://#{request.host}:#{request.port}/callback/")
    session[:oauth][:request_token] = @request_token.token
    session[:oauth][:request_token_secret] = @request_token.secret
    redirect @request_token.authorize_url
  end

  # http://<yourserver>/auth
  # Retrieves the @access_token then stores it inside a session cookie. In a real app, 
  # you'll want to persist the token in a datastore associated with the user.
  get "/callback/" do
    @access_token = @request_token.get_access_token :oauth_verifier => params[:oauth_verifier]
    session[:oauth][:access_token] = @access_token.token
    session[:oauth][:access_token_secret] = @access_token.secret
    redirect "/"
  end

  # http://<yourserver>/signout
  # Expires the session
  get "/signout" do
    session[:oauth] = {}
    @current_user = nil
    redirect "/"
  end

  # About page for GoStoryMap
  get '/hello' do
    erb :hello
  end

  # -- START APPLICATION --
  
  # http://<yourserver>/storyboard/<project.key>
  # Retrieves the story board for a particular JIRA project
  get '/storyboard/:key' do

    @project = MultiJson.decode(@access_token.get('/rest/api/2/project/' + params[:key],
                                                {'Accept' => 'application/json'}).body)
        
    @epics = MultiJson.decode(@access_token.get('/rest/api/2/search?jql=issuetype+%3D+Epic+AND+project+%3D+' + params[:key] + '&fields=summary,key',
                                                {'Accept' => 'application/json'}).body) 
    
    erb :storyboard
  end

  # POST to create an epic in the current project
  post '/create_epic' do

    @issuetype = "Epic"
  
    create_issue(params[:project_key], @issuetype, params[:summary], params[:description])

    redirect '/storyboard/' + params[:project_key]
  end

  # POST to create a story in the current project, linked to the selected epic
  post '/create_story' do

    issuetype = "Story"

    response = create_issue(params[:project_key], issuetype, params[:summary], params[:description])

    # issue has been created, now we need to find it and link it to the respective epic
    # Epic is the issue link name, 'contains' is what we will use for the display, and 'is part of' is what we will use for adding the story to the Epic

    story_json = MultiJson.decode(response)
    story_key = story_json['key']
    # link_to_epic(new_issue_key["key"], params[:epic_key])
    link_to_epic(story_key, params[:epic_key])
  
    redirect '/storyboard/' + params[:project_key]
  end

  # POST to link an existing story to a different epic
  post '/link_to_epic' do
  
    # remove_epic_link(params[:link_id])  
  
    # link_to_epic(params[:story_key], params[:epic_key])

    redirect '/storyboard/' + params[:project_key]
  end

  # Get a list of the stories for each epic
  def get_stories_by_epic(epic_key)
  
    MultiJson.decode(@access_token.get('/rest/api/2/search?jql=issue+in+linkedIssues(' + epic_key + ',contains)&fields=summary,key,issuelinks',
                                                {'Accept' => 'application/json'}).body)
  end

  def link_to_epic(story_key, epic_key)
  
    # @payload = {"type" => {"name" => "Epic"}, "inwardIssue" => {"key" => epic_key}, "outwardIssue" => {"key" => story_key}}
    # @payload = @payload.to_json

    # puts "Payload to be sent (link to epic): #{@payload}"

    # post("/issueLink/")

    MultiJson.encode(@access_token.post('/rest/api/2/issueLink/',
    '{
    "type": {
        "name": "Epic"
    },
    "inwardIssue": {
        "key": "' + epic_key + '"
    },
    "outwardIssue": {
        "key": "' + story_key + '"
    }}',
    {"Content-Type" => "application/json"}).body)

  end

  # Find the existing epic link and remove it
  def remove_epic_link(link_id)
    
    delete("/issueLink/" + link_id)
    
  end

  def create_issue(project_key, issuetype, summary, description)

    # MultiJson.decode(@access_token.post('/rest/api/2/issue/',
    #                                               {'Accept' => 'application/json'}).body) 

    # Generate post body (JSON) using the Light Protocol described here:
    # https://developer.atlassian.com/display/CONFDEV/Confluence+JSON-RPC+APIs#ConfluenceJSON-RPCAPIs-TheLightProtocol
    # payload = MultiJson.encode([page_id,{
    #   :fileName => file_name, 
    #   :pageId => page_id,
    #   :contentType => content_type
    # }, data])

    MultiJson.encode(@access_token.post('/rest/api/2/issue/',
      '{"fields":{
        "project" : 
          {"key" : "' + project_key + '"}, 
        "summary" : "' + summary + '", 
        "description" : "' + description + '", 
        "issuetype" : 
          {"name" : "' + issuetype + '"}
      }}',
    {"Content-Type" => "application/json"}).body)
  end
end