require 'rubygems'
require 'bundler/setup'
require 'rack/no-www'
require 'sinatra'
require 'sinatra/content_for'

Bundler.require(:default)

require "./gostorymap"

use Rack::NoWWW
use Rack::Static, :urls => [ "/font","/images", "/scripts","/css"], :root => "public"

run Gostorymap

