require 'jira'
require 'sinatra'
require 'sinatra/content_for'

class Gostorymap < Sinatra::Base
  enable :sessions

  # This section gets called before every request. Here, we set up the
  # OAuth consumer details including the consumer key, private key,
  # site uri, and the request token, access token, and authorize paths
  before do
    options = {
      :site               => 'http://localhost:8080',
      :signature_method   => 'RSA-SHA1',
      :request_token_path => "/plugins/servlet/oauth/request-token",
      :authorize_path     => "/plugins/servlet/oauth/authorize",
      :access_token_path  => "/plugins/servlet/oauth/access-token",
      :private_key_file   => "rsa.pem",
      :rest_base_path     => "/rest/api/2"
    }

	# 'gostorymap' is the Consumer Key of the Incoming Authentication in the JIRA Application Link
    @jira_client = JIRA::Client.new('gostorymap', '', options)
    @jira_client.consumer.http.set_debug_output($stderr)

    # Add AccessToken if authorised previously.
    if session[:jira_auth]
      @jira_client.set_access_token(
        session[:jira_auth][:access_token],
        session[:jira_auth][:access_key]
      )
    end
  end

  # Starting point: http://<yourserver>/
  # This will serve up a login link if you're not logged in. If you are, it'll show some user info and a
  # signout link
  get '/' do
    if !session[:jira_auth]
      # not logged in
	  erb :index
    else
      #logged in
      @issues = @jira_client.Issue.all
      @projects = @jira_client.Project.all
	  erb :home
    end
  end

  # http://<yourserver>/signin
  # Initiates the OAuth dance by first requesting a token then redirecting to 
  # http://<yourserver>/auth to get the @access_token
  get '/signin' do
    request_token = @jira_client.request_token
    session[:request_token] = request_token.token
    session[:request_secret] = request_token.secret

    redirect request_token.authorize_url    
  end

  # http://<yourserver>/callback/
  # Retrieves the @access_token then stores it inside a session cookie. In a real app, 
  # you'll want to persist the token in a datastore associated with the user.
  get "/callback/" do
    request_token = @jira_client.set_request_token(
      session[:request_token], session[:request_secret]
    )
    access_token = @jira_client.init_access_token(
      :oauth_verifier => params[:oauth_verifier]
    )

    session[:jira_auth] = {
      :access_token => access_token.token,
      :access_key => access_token.secret
    }

    session.delete(:request_token)
    session.delete(:request_secret)

    redirect "/"
  end

  # http://<yourserver>/signout
  # Expires the session
  get "/signout" do
    session.delete(:jira_auth)
    redirect "/"
  end
  
  # -- END AUTHENTICATION --
  
  # -- START APPLICATION --
  
  # http://<yourserver>/storyboard/<project.key>
  # Retrieves the story board for a particular JIRA project
  get '/storyboard/:key' do

	  @project = @jira_client.Project.find(params[:key])
        
    @epics = @jira_client.Issue.find("issuetype+%3D+Epic+AND+project+%3D+" + params[:key] + "&fields=summary,key")
    
    erb :storyboard
  end
  
  
  # About page for GoStoryMap
  get '/hello' do
    erb :hello
  end

  # Allows includes to be specified in erb's and placed in the layout.erb
  helpers Sinatra::ContentFor

   # Get a list of the stories for each epic
   def get_stories_by_epic(key)
 	
     @jira_client.Issue.all.find("issue+in+linkedIssues(" + params[:key] + ",contains)&fields=summary,key,issuelinks")
 	  
   end
   
  # -- END AUTHENTICATION --

end
